# Shell App in Kotlin

Create a project with a navigation drawer activity template.
The entry point is *MainActivity* as usual.
His view *layout/activity_main.xml* contains:
 * a `NavigationView` : the navigation drawer menu, configure with *menu/navigation_drawer.xml*
 * a `ToolBar` : the bar app with an option menu configure in *MainActivity*.
 * a `Framelayout` : the main content, *main_content*, of the app (include of *layout/content_main.xml*).

Add an option menu into the code by implementing `override fun onCreateOptionsMenu(menu: Menu): Boolean`.
Handle events on option menu with `override fun onOptionsItemSelected(item: MenuItem): Boolean`.

Handle events on navigation view menu with `override fun onNavigationItemSelected(item: MenuItem): Boolean`.

Add two simple `Fragment` named *MainFragment* and *ShellFragment* (code Kotlin + view).

Put the *MainFragment* in *main_content* via `suuportFragmentManager` and a
Kotlin extension function.
